var carousel = {
    app: {
        WIDTH: 800,
        HEIGHT: 500,
        carousel: $("#carousel"),
        slidesHolder: $('.slidesHolder'),
        counter: $("#carousel .counter"),
        prev: $('.controllers .prev'),
        next: $('.controllers .next'),
        current: 0,
        animationSpeed: 500,
        autoSpeed: 5000,
        interval: 0,
    },
    init: function(arr) {
        var carousel = this;
        carousel.app.length = arr.length;
        arr.forEach(function(item, index) {
            var img = document.createElement('img');
            $(img).attr("src", "./img/" + item + ".jpg");
            $(img).attr("data-order", index);
            $('.slidesHolder').append(img);
            $(img).css("left", index * carousel.app.WIDTH);
        });

        $(".container img").first().addClass('current');
        carousel.app.prev.addClass("noMore");
        carousel.updateData();
        carousel.listeners(); // invoke listeners function
    },
    updateData: function() {
        this.app.counter.html(this.app.current + 1 + "/" + this.app.length);
    },
    listeners: function() {
        var iterate = $("#carousel input[name='iterate']"),
            autoPlay = $("#carousel input[name='autoPlay']"),
            carousel = this,
            app = carousel.app;

        // 前一项
        app.prev.on("click", function() {
            if (app.isAutoPlay) {
                //如果自动播放，重启计时器
                clearInterval(app.interval);
                app.interval = setInterval(function() { carousel.move(app.current + 1); }, app.autoSpeed);
            }
            carousel.move(app.current - 1);
            carousel.updateData();
        });
        // 后一项
        app.next.on("click", function() {
            if (app.isAutoPlay) {
                //如果自动播放，重启计时器
                clearInterval(app.interval);
                app.interval = setInterval(function() { carousel.move(app.current + 1); }, app.autoSpeed);
            }
            carousel.move(app.current + 1);
            carousel.updateData();
        });
        // 是否循环播放
        iterate.on("change", function() {
            app.isIterate = $(this).is(":checked");

            // 判断是否要修改前后箭头class
            if (app.isIterate) {
                app.prev.removeClass("noMore");
                app.next.removeClass("noMore");
            }else if(app.current === 0 ){
            	app.prev.addClass("noMore");
            }else if(app.current === app.length -1){
            	app.next.addClass("noMore");
            }

        });
        // 是否自动播放
        autoPlay.on("change", function() {
            if ($(this).is(":checked")) {
                app.isAutoPlay = true;
                app.interval = setInterval(function() { carousel.move(app.current + 1); }, app.autoSpeed);
            } else {
                app.isAutoPlay = false;
                clearInterval(app.interval);
            }
        });
    },
    move: function(odr) {
        var app = this.app,
            container = app.slidesHolder;

        if (odr < 0 || odr >= app.length) {
            if (app.isIterate) {
                odr = odr < 0 ? app.length - 1 : 0; //跳转到最后一个或者第一个
            } else {
                return;
            }

        }

        // 如果图片到底并且不循环
        if (!app.isIterate) {
            if (odr === 0) {
                app.prev.addClass("noMore");
            } else {
                app.prev.removeClass("noMore");
            }

            if (odr === app.length - 1) {
                app.next.addClass("noMore");
            } else {
                app.next.removeClass("noMore");
            }
        }

        container.animate({ left: odr * this.app.WIDTH * -1 }, app.animationSpeed);
        app.current = odr;
        this.updateData();
    }
};


//Invoke carousel，读取img文件夹下面的jpg文件
var images = ["apple", "banana", "orange"];
carousel.init(images);